;;; juki4-theme.el --- A dark theme.

;;; Commentary:

;; A dark high contrast color theme.

;;; Code:
(deftheme juki4 "A dark high contrast color theme.")

(let ((font "Liberation Mono")
      (height 90;95;100
       )

      (black-1  "#000000")
      (black-2  "#222222")
      (black-3  "#333333")
      (black-4  "#444444")

      (white-1  "#FFFFFF")
      (white-2  "#AAAAAA")
      (white-3  "#888888")
      (white-4  "#555555")

      (red-1    "#ff9090")
      (red-2    "#c14d4d")
      (red-3    "#ba1414")
      (red-4    "#7c0202")

      (blue-1   "#50D0DB")
      (blue-2   "#3bafac")
      (blue-3   "#13726f")
      (blue-4   "#024240")

      (green-1  "#4EDF97")
      (green-2  "#42b77c")
      (green-3  "#0a6b3a")
      (green-4  "#02381d")

      (yellow-1 "#CcC242")
      (yellow-2 "#d8cd29")
      (yellow-3 "#726c12")
      (yellow-4 "#3d390b")

      (brown-1  "#fc9649")
      (brown-2  "#c47e48")
      (brown-3  "#823902")
      (brown-4  "#3a1a01")
      )
  (custom-theme-set-faces
   'juki4
   `(default                          ((t :background ,black-1;"grey10";,black-1
                                          :foreground "gainsboro";,white-1
                                          :font ,font
                                          :height ,height)))
   `(italic                           ((t :foreground ,white-1
                                          :slant italic)))
   `(cursor                           ((t :background ,white-1
                                          :foreground ,black-1)))
   `(fringe                           ((t :background ,black-2)))
   `(region                           ((t :background ,blue-4
                                          :foreground ,white-1)))
   `(secondary-selection              ((t :background ,blue-3)))
   `(mode-line-buffer-id              ((t :weight bold)))
   `(mode-line                        ((t :background ,white-2
                                          :foreground ,black-1)))
   `(mode-line-inactive               ((t :background ,white-3
                                          :foreground ,black-1)))
   `(minibuffer-prompt                ((t :foreground ,blue-1
                                          :weight bold)))
   `(shadow                           ((t :foreground ,white-3)))
   `(match                            ((t :background ,blue-1
                                          :foreground ,black-1)))
   `(highlight                        ((t :inherit region)))
   `(lazy-highlight                   ((t :background ,white-1
                                          :foreground ,black-1)))
   `(show-paren-match                 ((t :background ,yellow-1
                                          :foreground ,black-1)))
   `(success                          ((t :foreground ,green-1
                                          :weight bold)))
   `(warning                          ((t :foreground ,yellow-2
                                          :weight bold)))
   `(error                            ((t :inherit warning)))
   `(link                             ((t :foreground ,blue-1
                                          :underline ,blue-1)))
   `(link-visited                     ((t :foreground ,red-1
                                          :underline ,red-1)))
   `(button                           ((t :underline t)))
   `(header-line                      ((t :background ,black-2
                                          :weight bold)))

   ;; Isearch
   `(isearch                          ((t :background ,blue-1
                                          :foreground ,black-1)))
   `(isearch-fail                     ((t :background ,black-3)))

   ;; Font-lock
   `(font-lock-keyword-face           ((t :foreground ,blue-1
                                          :weight bold)))
   `(font-lock-builtin-face           ((t :foreground ,yellow-1)))
   `(font-lock-function-name-face     ((t :foreground ,red-1
                                          :weight bold)))
   `(font-lock-variable-name-face     ((t :foreground "#FF99FF"
                                          ;;:weight bold
                                        ;;:foreground ,red-1
                                          ;; :weight bold
                                          ;; :slant italic
                                          ;;:foreground ,blue-1
                                          ;;:weight bold
                                          )))
   `(font-lock-string-face            ((t :foreground ,green-1)))
   `(font-lock-doc-face               ((t :foreground ,green-2)))
   `(font-lock-type-face              ((t :foreground ,green-1
                                          :weight bold)))
   `(font-lock-comment-face           ((t :foreground ,brown-1)))
   `(font-lock-warning-face           ((t :foreground ,yellow-2
                                          :weight bold)))
   `(font-lock-constant-face          ((t :foreground ,yellow-1
                                          :weight bold)))

   ;; eshell
   `(eshell-prompt                    ((t :inherit minibuffer-prompt)))
   `(eshell-ls-backup                 ((t :inherit font-lock-comment-face)))

   ;; Sly
   `(sly-mrepl-output-face            ((t :foreground ,white-1)))
   `(sly-mode-line                    ((t :foreground ,black-1
                                          :weight bold)))

   ;; ediff
   `(ediff-current-diff-A             ((t :background ,red-4
                                          :foreground ,white-1)))
   `(ediff-current-diff-B             ((t :background ,green-4
                                          :foreground ,white-1)))
   `(ediff-current-diff-C             ((t :background ,brown-4
                                          :foreground ,white-1)))
   `(ediff-fine-diff-A                ((t :background ,red-2
                                          :foreground ,white-1)))
   `(ediff-fine-diff-B                ((t :background ,green-3
                                          :foreground ,white-1)))
   `(ediff-fine-diff-C                ((t :background ,brown-3
                                          :foreground ,white-1)))
   `(ediff-odd-diff-A                 ((t :background ,black-3)))
   `(ediff-odd-diff-B                 ((t :background ,black-3)))
   `(ediff-odd-diff-C                 ((t :background ,black-3)))
   `(ediff-even-diff-A                ((t :background ,black-3)))
   `(ediff-even-diff-B                ((t :background ,black-3)))
   `(ediff-even-diff-C                ((t :background ,black-3)))

   ;; Diff
   `(diff-removed                     ((t :background ,red-4
                                          :foreground ,white-1)))
   `(diff-added                       ((t :background ,green-4
                                          :foreground ,white-1)))
   
   ;; Magit
   `(magit-hash                       ((t :foreground ,white-3)))
   `(magit-section-highlight          ((t :background ,black-3)))
   `(magit-diff-added-highlight       ((t :background ,green-4
                                          :foreground ,white-1)))
   `(magit-diff-removed-highlight     ((t :background ,red-4
                                          :foreground ,white-1)))

   ;; Org
   `(org-agenda-date-today            ((t :foreground ,red-1)))
   `(org-column                       ((t :background ,black-1
                                          :underline nil)))
   `(org-block                        ((t :inherit default)))
   `(org-code                         ((t :foreground ,red-1)))
   `(org-verbatim                     ((t :foreground ,green-1)))
   `(outline-1                        ((t :inherit font-lock-function-name-face)))
   `(outline-2                        ((t :inherit font-lock-string-face)))
   `(outline-3                        ((t :inherit font-lock-variable-name-face)))
   `(outline-4                        ((t :inherit font-lock-builtin-face)))

   ;; Helm
   `(helm-selection                   ((t :background ,green-4
                                          :distant-foreground ,white-1
                                          :foreground ,white-1)))

   ;; Auto complete
   `(ac-selection-face                ((t :background ,blue-2
                                          :foreground ,black-1)))
   ))

(provide-theme 'juki4)
;;; juki4-theme.el ends here
